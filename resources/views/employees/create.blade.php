@extends('layouts.app')
@section('title','Новый сотрудник')
@section('content')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Новый сотрудник</li>
        </ol>
        <h2>Новый сотрудник</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <section id="employees" class="employees">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">

                    <h1 class="title">Создать нового сотрудника</h1>

                    @include('partials.site.errors')

                    <form method="post" action="{{ route('employees.store') }}">

                        @csrf
                        

                        <div class="form-group">
                            <label class="label">Имя</label>
                            <div class="control">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" class="input" placeholder="Имя" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Фамилия</label>
                            <div class="control">
                                <input type="text" class="form-control" name="surname" value="{{ old('surname') }}" class="input" placeholder="Фамилия" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Отчество</label>
                            <div class="control">
                                <input type="text" class="form-control" name="patronymic" value="{{ old('patronymic') }}" class="input" placeholder="Отчество" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Пол</label>
                            <div class="control">
                                <div class="select">
                                    <select class="form-control" name="sex" required>
                                        <option value="" disabled selected>Выберите пол</option>
                                        <option value="мужской" {{ old('sex') === 'мужской' ? 'selected' : null }}>мужской</option>
                                        <option value="женский" {{ old('sex') === 'женский' ? 'selected' : null }}>женский</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Заработная плата</label>
                            <div class="control">
                                <input type="number" class="form-control" name="salary" value="{{ old('salary') }}" class="input" placeholder="Зарплата" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Отделы</label>
                            <div class="control">
                                @foreach ($departments as $department)
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="departments[]" id="{{ $department->name }}" value="{{ $department->id }}" /> {{ $department->name }}
                                    </label>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="control">
                                <button type="submit" class="btn btn-primary">Создать</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->

@endsection