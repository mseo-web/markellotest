@extends('layouts.app')
@section('title','Сотрудники')
@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Сотрудники</li>
        </ol>
        <h2>Сотрудники</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= employees Section ======= -->
    <section id="employeeslist" class="employees">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-12">
            <div class="fl jc-sa ai-c">
                <div class=""><h1>Сотрудники</h1></div>
                <div class=""><a href="{{ route('employees.create')}}" class="btn btn-primary">Создать</a></div>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                
                <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                          <tr>
                            <td>Имя</td>
                            <td>Фамилия</td>
                            <td>Отчество</td>
                            <td>Пол</td>
                            <td>Заработная плата</td>
                            <td>Названия отделов</td>
                            <td></td>
                            <td></td>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($employees as $employee)
                          <tr>
                              <td>{{$employee->name}}</td>
                              <td>{{$employee->surname}}</td>
                              <td>{{$employee->patronymic}}</td>
                              <td>{{$employee->sex}}</td>
                              <td>{{$employee->salary}}</td>
                              @php $empldepartments = explode(",", $employee->departments); @endphp
                              @php $arrdepartments=[]; @endphp
                              @for($n = 0; $n < count($empldepartments); $n++)
                                @foreach($departments as $department)
                                  @if($empldepartments[$n]==$department->id)
                                    @php array_push($arrdepartments, $department->name); @endphp
                                  @endif
                                @endforeach
                              @endfor
                              @php $arrdep=implode(",",$arrdepartments); @endphp
                              <td>
                                {{$arrdep}}
                              </td>
                              <td>
                                  <a href="{{ route('employees.edit',$employee->id)}}" class="btn btn-primary theme-btn">Редактировать</a>
                              </td>
                              <td>
                                  <form action="{{ route('employees.destroy', $employee->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger theme-btn" type="submit">Удалить</button>
                                  </form>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                    </table>
                </div>
            <div>
        </div>

      </div>
    </section><!-- End employees Section -->

</main><!-- End #main -->

@endsection