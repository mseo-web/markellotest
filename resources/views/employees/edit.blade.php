@extends('layouts.app')
@section('title','Редактирование')
@section('content')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Редактирование</li>
        </ol>
        <h2>Изменить информацию о сотруднике</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <section id="employees" class="employees">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">

                    <h1 class="title">Изменить информацию о сотруднике</h1>

                    @include('partials.site.errors')

                    <form method="POST" action="{{ route('employees.update',$employee->id) }}">

                        @csrf
                        @method('PUT')
                        

                        <div class="form-group">
                            <label class="label">Имя</label>
                            <div class="control">
                                <input type="text" class="form-control" name="name" value="{{ $employee->name }}" class="input" placeholder="Имя" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Фамилия</label>
                            <div class="control">
                                <input type="text" class="form-control" name="surname" value="{{ $employee->surname }}" class="input" placeholder="Фамилия" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Отчество</label>
                            <div class="control">
                                <input type="text" class="form-control" name="patronymic" value="{{ $employee->patronymic }}" class="input" placeholder="Отчество" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Пол</label>
                            <div class="control">
                                <div class="select">
                                    <select class="form-control" name="sex" required>
                                        <option value="" disabled selected>Выберите пол</option>
                                        <option value="мужской" {{ $employee->sex === 'мужской' ? 'selected' : null }}>мужской</option>
                                        <option value="женский" {{ $employee->sex === 'женский' ? 'selected' : null }}>женский</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Заработная плата</label>
                            <div class="control">
                                <input type="number" class="form-control" name="salary" value="{{ $employee->salary }}" class="input" placeholder="Зарплата" required />
                            </div>
                        </div>

                          @php
                            $departments_array =  $employee->departments;
                            $departments_arr = explode(",", $departments_array);
                          @endphp

                        <div class="form-group">
                            <label class="label">Отделы</label>
                            <div class="control">
                                @foreach ($departments as $department)
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="departments[]" id="" value="{{ $department->id }}" {{in_array($department->id, $departments_arr) ? 'checked' : ''}} /> {{ $department->name }}
                                    </label>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <input type="hidden" name="id" value = "{{ $employee->id }}">

                        <div class="form-group">
                            <div class="control">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->

@endsection