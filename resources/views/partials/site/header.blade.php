<div id="loading-overlay">
    <div class="loader"></div>
</div> <!-- /.loading-overlay -->

<!-- ======= Top Bar ======= -->
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">mihas-ox7-74@mail.ru</a>
        <i class="icofont-phone"></i> +77088210530
      </div>
      <div class="social-links">
        <!-- <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
        <a href="#">Понедельник - Пятница: 09.00 - 18.00</a>
      </div>
    </div>
</section>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <!-- <h1 class="text-light"><a href="index.html"><span>SMARTEXWEB</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
         <a href="{{url('/')}}"><img src="{{ asset('public/assets/img/SM-logo1.png') }}" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="{{url('/')}}" title="">ГЛАВНАЯ</a></li>

          <li><a href="{{url('/departments')}}" title="">ОТДЕЛЫ</a>

          <li><a href="{{url('/employees')}}" title="">СОТРУДНИКИ</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->