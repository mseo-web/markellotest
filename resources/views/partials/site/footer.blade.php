<!-- ======= Footer ======= -->
  <footer id="footer">

    <!-- <div class="footer-newsletter">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
          </div>
          <div class="col-lg-6">
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div> -->

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-links">
            <div id="logo-ft">
                <a href="{{url('/')}}"><img src="{{ asset('public/assets/img/SM-logo2.png') }}" alt="logo-ft"  width="128" data-retina="{{ asset('public/assets/img/SM-logo2.png') }}" data-width="128"></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="{{url('/')}}">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{url('/departments')}}">Отделы</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="{{url('/employees')}}">Сотрудники</a></li>
            </ul>
          </div>

          <!-- <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div> -->

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Контакты</h4>
            <p>
              Республика Казахстан, <br>
              г. Павлодар <br><br>
              <strong>Телефон:</strong> +77088210530<br>
              <strong>Email:</strong> mihas-ox7-74@mail.ru<br>
            </p>

          </div>

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>Smartexweb</h3>
            <p>Создание сайтов, мобильных приложений, десктопных приложений.</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Smartexweb</span></strong>. Все права защищены
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
        <!-- Разработка сайта <a href="https://smartexweb.kz/" target="_blank">SMARTEXWEB</a><br> -->
        <!-- Designed by <a href="https://bootstrapmade.com/" target="_blank">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->