@extends('layouts.app')
@section('title','Главная')
@section('content')

  <main id="main">

    <!-- ======= Setka Section ======= -->
    <section id="setka" class="setka">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-12">
            <div class="mt-40 t-center">
              <h1>Тестовое задание</h1>
              <h2>Сетка</h2>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
            <div class="col-sm-12">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td></td>
                                @foreach($departments as $department)
                                <td>{{$department->name}}</td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                              <td>{{$employee->name}} {{$employee->surname}}</td>

                              @php $empldepartments = explode(",", $employee->departments); @endphp
                              @php $arrdepartments=[]; @endphp

                              @for($n = 0; $n < count($empldepartments); $n++)
                                @foreach($departments as $department)
                                  @if($empldepartments[$n]==$department->id)
                                    @php array_push($arrdepartments, $department->name); @endphp
                                  @endif
                                @endforeach
                              @endfor

                              @php $arrdep=implode(",",$arrdepartments); @endphp

                              @for ($i = 0; $i < count($departments); $i++)
                                @if(strpos($arrdep,$departments[$i]['name']) !== false)
                                <td><i class="icofont-verification-check"></i></td>
                                @else
                                <td></td>
                                @endif
                              @endfor
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <div>
        </div><!-- row -->

      </div>
    </section><!-- End Setka Section -->

  </main><!-- End #main -->

@endsection