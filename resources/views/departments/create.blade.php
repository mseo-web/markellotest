@extends('layouts.app')
@section('title','Новый отдел')
@section('content')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Новый отдел</li>
        </ol>
        <h2>Новый отдел</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <section id="employees" class="employees">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">

                    <h1 class="title">Создать новый отдел</h1>

                    @include('partials.site.errors')

                    <form method="post" action="{{ route('departments.store') }}">

                        @csrf

                        <div class="form-group">
                            <label class="label">Название</label>
                            <div class="control">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" class="input" placeholder="Название" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="control">
                                <button type="submit" class="btn btn-primary">Создать</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->

@endsection