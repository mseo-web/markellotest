@extends('layouts.app')
@section('title','Редактирование')
@section('content')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Редактирование</li>
        </ol>
        <h2>Изменить информацию об отделе</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <section id="employees" class="employees">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">

                    <h1 class="title">Изменить информацию об отделе</h1>

                    @include('partials.site.errors')

                    <form method="post" action="{{ route('departments.update',$department->id) }}">

                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label class="label">Название</label>
                            <div class="control">
                                <input type="text" class="form-control" name="name" value="{{ $department->name }}" class="input" placeholder="Название" required />
                            </div>
                        </div>

                        <input type="hidden" name="id" value = "{{ $department->id }}">

                        <div class="form-group">
                            <div class="control">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->

@endsection