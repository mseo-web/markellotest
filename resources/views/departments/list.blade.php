@extends('layouts.app')
@section('title','Отделы')
@section('content')

<main id="main">

  <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{url('/')}}">Главная</a></li>
          <li>Отделы</li>
        </ol>
        <h2>Отделы</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= departments Section ======= -->
    <section id="departmentslist" class="departments">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-12">
            <div class="fl jc-sa ai-c">
                <div class=""><h1>Отделы</h1></div>
                <div class=""><a href="{{ route('departments.create')}}" class="btn btn-primary">Создать</a></div>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

              @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                      <p>{{ $message }}</p>
                  </div>
              @endif
              
                <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                          <tr>
                            <td>Название отдела</td>
                            <td>Количество сотрудников отдела</td>
                            <td>Максимальная заработная плата среди сотрудников отдела</td>
                            <td></td>
                            <td></td>
                          </tr>
                      </thead>
                      <tbody>
                          @for($i = 0; $i < count($departments); $i++)
                          <tr>
                              <td>{{$departments[$i]['name']}}</td>
                              @php $x=0; @endphp
                              @foreach($employees as $employee)
                                @php $empldepartments = explode(",", $employee->departments); @endphp
                                @for($m = 0; $m < count($empldepartments); $m++)
                                  @if($empldepartments[$m]==$departments[$i]['id'])
                                    @php $x=$x+1; @endphp
                                  @endif
                                @endfor
                              @endforeach
                              <td>
                                @php echo $x; @endphp
                              </td>
                                @php $salaries=[]; @endphp
                                @foreach($employees as $employee)
                                  @php $empldepartments = explode(",", $employee->departments); @endphp
                                  @for($m = 0; $m < count($empldepartments); $m++)
                                    @if($empldepartments[$m]==$departments[$i]['id'])
                                      @php array_push($salaries, $employee->salary); @endphp
                                    @endif
                                  @endfor
                                @endforeach
                              <td>
                                @if(count($salaries)!==0) 
                                    @php echo max($salaries); @endphp
                                @else 
                                    @php echo '0'; @endphp
                                @endif
                              </td>
                              <td>
                                  <a href="{{ route('departments.edit',$departments[$i]['id'])}}" class="btn btn-primary theme-btn">Редактировать</a>
                              </td>
                              <td>
                                  <form action="{{ route('departments.destroy', $departments[$i]['id'])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger theme-btn" type="submit" @php if($x!=0) echo 'disabled' @endphp>Удалить</button>
                                  </form>
                              </td>
                          </tr>
                          @endfor
                      </tbody>
                    </table>
                </div>
            <div>
        </div>

      </div>
    </section><!-- End employees Section -->

</main><!-- End #main -->

@endsection
