<!DOCTYPE html>
<html lang="en-US">
    <head>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>

        <title>SMARTEXWEB - @yield('title')</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{ asset('public/assets/img/favicon.png') }}" rel="icon">
        <link href="{{ asset('public/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{ asset('public/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="{{ asset('public/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('public/assets/css/app.css') }}" rel="stylesheet">
    </head>
    <body>

        @include('partials.site.header')

    	@yield('content')

        @include('partials.site.footer')

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
        
        <!-- Vendor JS Files -->
        <script src="{{ asset('public/assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
        <!-- <script src="{{ asset('public/assets/vendor/php-email-form/validate.js') }}"></script> -->
        <script src="{{ asset('public/assets/vendor/jquery-sticky/jquery.sticky.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/counterup/counterup.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/venobox/venobox.min.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/wow/wow.js') }}"></script>

        <!-- Template Main JS File -->
        <script src="{{ asset('public/assets/js/main.js') }}"></script>

	</body>
</html>