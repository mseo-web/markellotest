<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Department;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $departments = Department::all();

        return view('employees.list', compact('employees'), compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view('employees.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'surname' => 'required|string',
            'salary' => 'required|integer',
            'departments' => 'required'
        ]);

        $departments_arr = $request->get('departments');

        $patronymic = '';
        if($request->get('patronymic')===null){
            $patronymic = '';
        }else{
            $patronymic = $request->get('patronymic');
        }

        $employee = new Employee();
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->patronymic = $patronymic;
        $employee->sex = $request->get('sex');
        $employee->salary = $request->get('salary');
        $employee->departments = implode(",",$departments_arr);
        $employee->save();
        return redirect('/employees')->with('success', 'Сотрудник создан!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $departments = Department::all();
        // $employee = Employee::find($id);
        return view('employees.edit', compact('employee'), compact('departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'surname' => 'required|string',
            'salary' => 'required|integer',
            'departments' => 'required'
        ]);

        $departments_arr = $request->get('departments');

        $patronymic = '';
        if($request->get('patronymic')===null){
            $patronymic = '';
        }else{
            $patronymic = $request->get('patronymic');
        }

        $employee = Employee::find($request->input('id'));
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->patronymic = $patronymic;
        $employee->sex = $request->get('sex');
        $employee->salary = $request->get('salary');
        $employee->departments = implode(",",$departments_arr);
        $employee->save();
        
        return redirect('/employees')->with('success', 'Информация обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return redirect('/employees')->with('success', 'Сотрудник удалён!');
    }
}
