<?php

namespace App\Http\Controllers;

use App\Home;
use App\Employee;
use App\Department;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	$employees = Employee::all();
    	$departments = Department::all();
        return view('home', compact('employees'), compact('departments'));
    }
}
